package org.hsmith.kserver.scheduler.context

import java.util.*
import org.hsmith.hibernate_decorator.api.DbSession
import org.hsmith.kserver.api.KServer
import org.quartz.JobExecutionContext

/**
 * Context object used to provide data relevant to each job execution.
 */
interface ScheduledJobContext {
    /**
     * Raw Quartz [JobExecutionContext] object. Includes data such as the [org.quartz.JobDetail], [org.quartz.Trigger],
     * scheduled execution time, actual execution time, etc.
     */
    val jobExecutionContext: JobExecutionContext

    /**
     * [KServer] instance this job is running on.
     * This interface allows starting and stopping the server instance.
     *
     * Note that once the server is stopped, all pending scheduled jobs are dropped.
     */
    val server: KServer

    /**
     * Optional database session from the `hibernate-decorator` library.
     * If a [org.hsmith.hibernate_decorator.api.DbApi] was provided when the [org.hsmith.kserver.api.KServer]
     * was build, each scheduled job execution will be provided with a fresh database session. Otherwise, this property
     * will always be empty.
     *
     * Note that the database session is automatically closed when the job execution is complete.
     *
     * See [org.hsmith.kserver.api.KServerBuilder.withDbApi].
     */
    val dbSession: Optional<DbSession>
}
