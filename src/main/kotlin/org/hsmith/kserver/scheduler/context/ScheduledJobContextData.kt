package org.hsmith.kserver.scheduler.context

import java.util.*
import org.hsmith.hibernate_decorator.api.DbSession
import org.hsmith.kserver.api.KServer
import org.quartz.JobExecutionContext

internal data class ScheduledJobContextData(
    override val jobExecutionContext: JobExecutionContext,
    override val server: KServer,
    override val dbSession: Optional<DbSession>
) : ScheduledJobContext
