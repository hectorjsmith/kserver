package org.hsmith.kserver.scheduler.scheduler

import org.hsmith.kserver.scheduler.base.ScheduledJob
import org.hsmith.kserver.scheduler.manager.ScheduledJobManager

internal class JobSchedulerImpl(
    private val scheduledJobManager: ScheduledJobManager
) : JobScheduler {
    override fun scheduleJob(job: ScheduledJob) {
        scheduledJobManager.registerNewJob(job)
    }
}
