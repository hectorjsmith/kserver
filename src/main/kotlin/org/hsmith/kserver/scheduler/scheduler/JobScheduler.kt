package org.hsmith.kserver.scheduler.scheduler

import org.hsmith.kserver.scheduler.base.ScheduledJob

/**
 * Interface used to schedule a job.
 * Jobs are scheduled in the same way as when scheduled using the
 * [org.hsmith.kserver.api.KServerBuilder.withScheduledJob] method.
 */
interface JobScheduler {

    /**
     * Schedule a new job to be run at a certain time.
     * Jobs are scheduled in the same way as when scheduled using the
     * [org.hsmith.kserver.api.KServerBuilder.withScheduledJob] method.
     *
     * Note that registering a job with a duplicate group and name will throw an error.
     * These fields must be unique.
     */
    fun scheduleJob(job: ScheduledJob)
}
