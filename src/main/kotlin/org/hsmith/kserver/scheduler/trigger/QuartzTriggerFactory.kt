package org.hsmith.kserver.scheduler.trigger

import java.time.ZonedDateTime
import java.util.*
import org.quartz.Trigger

/**
 * Factory object to handle creating job triggers for scheduled jobs.
 *
 * See [org.hsmith.kserver.api.KServerApi.newQuartzTriggerFactory] to get an instance of this interface.
 */
interface QuartzTriggerFactory {
    /**
     * Build a new job trigger that fires a job a single time at the requested execution time.
     */
    fun newOnceOffTrigger(jobName: String, jobGroup: String, executionTime: ZonedDateTime): Trigger

    /**
     * Build a new job trigger that fires a job a single time a certain number of milliseconds from now.
     */
    fun newOnceOffTrigger(jobName: String, jobGroup: String, milliSecondsFromNow: Long): Trigger

    /**
     * Build a new job trigger that fires a job at a consistent interval.
     *
     * The first execution is delayed by the given number of milliseconds. After the first execution, the job
     * fires repeatedly with the provided number of milliseconds between each execution.
     *
     * Note that a job with this trigger will fire an infinite number of times.
     */
    fun newSimpleRecurringTrigger(jobName: String, jobGroup: String, startAfterMs: Long, repeatEveryMs: Long): Trigger

    /**
     * Build a new job trigger that fires based on a cron expression.
     *
     * The cron expression is assumed to be valid. A runtime exception is thrown if it is not.
     *
     * See [Quartz documentation](http://www.quartz-scheduler.org/documentation/quartz-2.3.0/tutorials/crontrigger.html)
     * for more information on cron scheduling.
     */
    fun newCrontabTrigger(jobName: String, jobGroup: String, cronExpression: String): Trigger
}
