package org.hsmith.kserver.scheduler.trigger

import java.time.ZonedDateTime
import java.util.*
import org.quartz.CronScheduleBuilder
import org.quartz.SimpleScheduleBuilder
import org.quartz.Trigger
import org.quartz.TriggerBuilder

internal class QuartzTriggerFactoryImpl : QuartzTriggerFactory {
    override fun newOnceOffTrigger(jobName: String, jobGroup: String, executionTime: ZonedDateTime): Trigger {
        return TriggerBuilder.newTrigger()
                .withIdentity(getTriggerNameWithRandomId(jobName), jobGroup)
                .startAt(Date.from(executionTime.toInstant()))
                .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                        .withMisfireHandlingInstructionFireNow())
                .build()
    }

    override fun newOnceOffTrigger(jobName: String, jobGroup: String, milliSecondsFromNow: Long): Trigger {
        return newOnceOffTrigger(jobName, jobGroup, ZonedDateTime.now().plusNanos(milliSecondsFromNow * 1_000_000))
    }

    override fun newSimpleRecurringTrigger(jobName: String, jobGroup: String, startAfterMs: Long, repeatEveryMs: Long): Trigger {
        return TriggerBuilder.newTrigger()
                .withIdentity(getTriggerNameWithRandomId(jobName), jobGroup)
                .startAt(Date.from(ZonedDateTime.now().plusNanos(startAfterMs * 1_000_000).toInstant()))
                .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                        .withIntervalInMilliseconds(repeatEveryMs)
                        .repeatForever()
                        .withMisfireHandlingInstructionNextWithRemainingCount())
                .build()
    }

    override fun newCrontabTrigger(jobName: String, jobGroup: String, cronExpression: String): Trigger {
        return TriggerBuilder.newTrigger()
                .withIdentity(getTriggerNameWithRandomId(jobName), jobGroup)
                .withSchedule(CronScheduleBuilder
                        .cronSchedule(cronExpression))
                .build()
    }

    /**
     * Generate a name for a job trigger: [jobname].trigger.[id]
     */
    private fun getTriggerNameWithRandomId(jobName: String): String {
        return "$jobName.trigger.${generateRandomTriggerId()}"
    }

    private fun generateRandomTriggerId(): String {
        return UUID.randomUUID().toString()
    }
}
