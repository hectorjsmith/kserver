package org.hsmith.kserver.scheduler.base

import org.quartz.JobDetail

/**
 * Wrapper class to handle building the Quartz [JobDetail] object for the job to be scheduled, and building the
 * [org.hsmith.kserver.scheduler.context.ScheduledJobContext] for each job execution.
 */
internal interface ScheduledJobWrapper {
    /**
     * [JobDetail] instance used by Quartz to schedule jobs.
     * This object is built based on the perperties defined in the [ScheduledJob] this interface wraps.
     */
    val jobDetail: JobDetail
}
