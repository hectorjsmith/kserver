package org.hsmith.kserver.scheduler.base

import org.hsmith.kserver.scheduler.context.ScheduledJobContext
import org.quartz.Trigger

/**
 * Scheduled job.
 * Implementations of this interface represent jobs to be executed at a scheduled time based on the list of triggers
 * they provide.
 */
interface ScheduledJob {
    /**
     * Name of this job.
     * The [jobName] is combined with the [jobGroup] to calculate the job [org.quartz.JobKey].
     */
    val jobName: String

    /**
     * Group this job belongs to.
     * The [jobName] is combined with the [jobGroup] to calculate the job [org.quartz.JobKey].
     */
    val jobGroup: String

    /**
     * List of Quartz triggers that fire this job.
     *
     * See [org.hsmith.kserver.scheduler.trigger.QuartzTriggerFactory] for creating triggers.
     */
    val jobTriggers: List<Trigger>

    /**
     * Function that is called when the job is executed.
     * A [ScheduledJobContext] instance is provided for each job execution.
     */
    fun run(context: ScheduledJobContext)
}
