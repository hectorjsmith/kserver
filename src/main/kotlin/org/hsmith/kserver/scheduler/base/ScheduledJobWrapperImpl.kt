package org.hsmith.kserver.scheduler.base

import java.util.*
import org.hsmith.hibernate_decorator.api.DbApi
import org.hsmith.kserver.api.KServer
import org.hsmith.kserver.scheduler.context.ScheduledJobContext
import org.hsmith.kserver.scheduler.context.ScheduledJobContextData
import org.quartz.*

internal class ScheduledJobWrapperImpl(
    private val job: ScheduledJob,
    private val server: Optional<KServer>,
    private val dbApi: Optional<DbApi>
) : Job, ScheduledJobWrapper, ScheduledJob by job {

    override val jobDetail: JobDetail by lazy { buildJobDetail() }

    // Empty constructor for quartz - this constructor is used when the job is executed
    constructor() : this(NotImplementedScheduledJob(), Optional.empty(), Optional.empty())

    @Throws(JobExecutionException::class)
    override fun execute(jobExecutionContext: JobExecutionContext) {
        val jobDetail = jobExecutionContext.jobDetail
        try {
            val internalContext = getInternalJobContext(jobDetail)
            val jobContext = buildScheduledJobContext(jobExecutionContext, internalContext)
            internalContext.job.run(jobContext)
            jobContext.dbSession.ifPresent { it.close() }
        } catch (e: Exception) {
            throw JobExecutionException("Exception in job: ${jobDetail.key}", e)
        }
    }

    private fun buildJobDetail(): JobDetail {
        val jobDetail = JobBuilder.newJob(javaClass)
                .withIdentity(jobName, jobGroup)
                .build()

        saveInternalJobContext(jobDetail)
        return jobDetail
    }

    private fun buildScheduledJobContext(
        jobExecutionContext: JobExecutionContext,
        internalContext: InternalJobDataContext
    ): ScheduledJobContext {
        val dbSession = Optional.ofNullable(internalContext.dbApi.orElse(null)?.newSession())
        return ScheduledJobContextData(jobExecutionContext, internalContext.server, dbSession)
    }

    private fun saveInternalJobContext(jobDetail: JobDetail) {
        val ctx = InternalJobDataContext(job, server.get(), dbApi)
        jobDetail.jobDataMap[InternalJobDataContext.CONTEXT_KEY] = ctx
    }

    private fun getInternalJobContext(executionJobDetail: JobDetail): InternalJobDataContext {
        val obj = executionJobDetail.jobDataMap[InternalJobDataContext.CONTEXT_KEY]
        if (obj is InternalJobDataContext) {
            return obj
        }
        throw IllegalArgumentException("Invalid job context object found")
    }

    private data class InternalJobDataContext(
        val job: ScheduledJob,
        val server: KServer,
        val dbApi: Optional<DbApi>
    ) {
        companion object {
            const val CONTEXT_KEY = "job_ctx"
        }
    }

    private class NotImplementedScheduledJob : ScheduledJob {
        override val jobName: String
            get() = TODO("Not yet implemented")
        override val jobGroup: String
            get() = TODO("Not yet implemented")
        override val jobTriggers: List<Trigger>
            get() = TODO("Not yet implemented")

        override fun run(context: ScheduledJobContext) {
            TODO("Not yet implemented")
        }
    }
}
