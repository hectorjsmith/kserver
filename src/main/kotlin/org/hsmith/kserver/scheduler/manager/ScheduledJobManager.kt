package org.hsmith.kserver.scheduler.manager

import org.hsmith.kserver.scheduler.base.ScheduledJob

/**
 * Manager class that handles setting up, starting, and stopping the Quartz scheduler.
 */
internal interface ScheduledJobManager {
    /**
     * Flag to check if the Quartz scheduler is running or not.
     */
    val isRunning: Boolean

    /**
     * Start the Quartz job scheduler.
     * This will setup the scheduler and register all provided jobs.
     */
    fun start()

    /**
     * Stop the Quartz scheduler.
     * This will stop the scheduler without waiting for any running job to complete.
     * Any jobs waiting to be executed will be removed and no more jobs will fire.
     */
    fun stop()

    /**
     * Register a new scheduled job to be executed.
     *
     * If the scheduler is running, the job is scheduled immediately and will fire as defined by the job triggers.
     * If the scheduler is not running, the job will not be immediately scheduled. Instead it will be stored in a
     * collection of jobs to schedule once the scheduler is started.
     */
    fun registerNewJob(job: ScheduledJob)
}
