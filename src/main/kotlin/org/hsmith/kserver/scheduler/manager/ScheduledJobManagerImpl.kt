package org.hsmith.kserver.scheduler.manager

import java.util.*
import org.hsmith.hibernate_decorator.api.DbApi
import org.hsmith.kserver.api.KServer
import org.hsmith.kserver.scheduler.base.ScheduledJob
import org.hsmith.kserver.scheduler.base.ScheduledJobWrapperImpl
import org.quartz.Scheduler
import org.quartz.SchedulerException
import org.quartz.impl.StdSchedulerFactory
import org.slf4j.LoggerFactory

internal class ScheduledJobManagerImpl(
    private val server: KServer,
    private val dbApi: Optional<DbApi>
) : ScheduledJobManager {
    private val logger = LoggerFactory.getLogger(this::class.java)
    private val registeredJobs = mutableListOf<ScheduledJob>()
    private val scheduler: Scheduler by lazy { buildScheduler() }

    override var isRunning: Boolean = false
        private set

    override fun registerNewJob(job: ScheduledJob) {
        if (isRunning) {
            try {
                scheduleJob(job)
            } catch (e: SchedulerException) {
                logger.error("Failed to schedule job ${job.jobName}", e)
            }
        } else {
            registeredJobs.add(job)
        }
    }

    override fun start() {
        if (isRunning) {
            logger.warn("Scheduler already running")
            return
        }
        try {
            logger.info("Starting job scheduler ...")
            setupSchedulerWithRegisteredJobs()
            scheduler.start()
            isRunning = true
        } catch (e: SchedulerException) {
            logger.error("Failed to start recurring job scheduler", e)
        }
    }

    override fun stop() {
        if (!isRunning) {
            logger.warn("Scheduler already stopped")
            return
        }
        try {
            logger.info("Stopping job scheduler ...")
            scheduler.clear()
            scheduler.shutdown(false)
        } catch (e: SchedulerException) {
            logger.error("Failed to stop job scheduler", e)
        }
        isRunning = false
    }

    private fun setupSchedulerWithRegisteredJobs() {
        try {
            scheduleRegisteredJobs()
        } catch (e: SchedulerException) {
            logger.error("Failed to setup job scheduler", e)
        }
    }

    @Throws(SchedulerException::class)
    private fun buildScheduler(): Scheduler {
        return StdSchedulerFactory.getDefaultScheduler()
    }

    @Throws(SchedulerException::class)
    private fun scheduleRegisteredJobs() {
        for (job in registeredJobs) {
            scheduleJob(job)
        }
    }

    @Throws(SchedulerException::class)
    private fun scheduleJob(job: ScheduledJob) {
        val wrapper = ScheduledJobWrapperImpl(job, Optional.of(server), dbApi)
        val jobId = "${wrapper.jobGroup}.${wrapper.jobName}"

        logger.info("Scheduling job: $jobId")
        if (wrapper.jobTriggers.isEmpty()) {
            logger.warn("Cannot schedule job, no triggers defined for job $jobId")
            return
        }

        scheduler.scheduleJobs(mapOf(Pair(wrapper.jobDetail, wrapper.jobTriggers.toSet())), false)
    }
}
