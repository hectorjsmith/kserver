package org.hsmith.kserver.api

import org.hsmith.hibernate_decorator.api.DbApi
import org.hsmith.kserver.javalin.config.JavalinConfigurator
import org.hsmith.kserver.javalin.handler.WebRequestHandler
import org.hsmith.kserver.scheduler.base.ScheduledJob

/**
 * Builder class responsible for building instances of [KServer].
 */
interface KServerBuilder {
    /**
     * Set the port used by the [io.javalin.Javalin] server.
     */
    fun withServerPort(port: Int): KServerBuilder

    /**
     * Set the [DbApi] instance to used by this [KServer].
     *
     * If an instance is provided, web request handlers and scheduled job handlers will automatically be provided with
     * an open [org.hsmith.hibernate_decorator.api.DbSession] instance.
     */
    fun withDbApi(dbApi: DbApi): KServerBuilder

    /**
     * Set an object to be used when configuring the [io.javalin.Javalin] server before startup.
     */
    fun withJavalinConfigurator(javalinConfigurator: JavalinConfigurator): KServerBuilder

    /**
     * Add a web request handler to be registered on the [io.javalin.Javalin] server.
     */
    fun withRequestHandler(handler: WebRequestHandler): KServerBuilder

    /**
     * Add a collection of web request handlers. See [withRequestHandler].
     */
    fun withRequestHandler(handlers: Iterable<WebRequestHandler>): KServerBuilder

    /**
     * Add a scheduled job to be executed by the Quartz executor.
     */
    fun withScheduledJob(job: ScheduledJob): KServerBuilder

    /**
     * Add a collection of scheduled jobs to be executed by the Quartz executor.
     *
     * See [withScheduledJob].
     */
    fun withScheduledJob(jobs: Iterable<ScheduledJob>): KServerBuilder

    /**
     * Build a new [KServer] instance with the options provided to this builder.
     */
    fun build(): KServer
}
