package org.hsmith.kserver.api.impl

import io.javalin.Javalin
import java.util.*
import org.hsmith.hibernate_decorator.api.DbApi
import org.hsmith.kserver.api.KServer
import org.hsmith.kserver.javalin.config.DefaultJavalinConfigurator
import org.hsmith.kserver.javalin.config.JavalinConfigurator
import org.hsmith.kserver.javalin.handler.RequestHandlerWrapperWithContext
import org.hsmith.kserver.javalin.handler.WebRequestHandler
import org.hsmith.kserver.scheduler.base.ScheduledJob
import org.hsmith.kserver.scheduler.manager.ScheduledJobManager
import org.hsmith.kserver.scheduler.manager.ScheduledJobManagerImpl
import org.hsmith.kserver.scheduler.scheduler.JobScheduler
import org.hsmith.kserver.scheduler.scheduler.JobSchedulerImpl
import org.slf4j.LoggerFactory

internal class KServerImpl(
    private val serverPort: Optional<Int>,
    private val dbApi: Optional<DbApi>,
    private val javalinConfigurator: Optional<JavalinConfigurator>,
    private val webRequestHandlers: Iterable<WebRequestHandler>,
    private val scheduledJobs: Iterable<ScheduledJob>
) : KServer {
    private val logger = LoggerFactory.getLogger(this::class.java)
    private val defaultJavalinConfigurator: JavalinConfigurator = DefaultJavalinConfigurator()
    private val javalinServer: Javalin by lazy { buildJavalinServer() }
    private val scheduledJobManager: ScheduledJobManager by lazy { buildScheduledJobManager() }
    private val jobScheduler: Optional<JobScheduler> by lazy { Optional.of(JobSchedulerImpl(scheduledJobManager)) }

    override var isRunning: Boolean = false
        private set

    override fun start() {
        if (isRunning) {
            logger.warn("Server is already running")
        } else {
            serverPort.ifPresentOrElse({ javalinServer.start(it) }, { javalinServer.start() })
            scheduledJobManager.start()
            isRunning = true
        }
    }

    override fun stop() {
        if (isRunning) {
            javalinServer.stop()
            scheduledJobManager.stop()
            isRunning = false
        } else {
            logger.warn("Server is already stopped")
        }
    }

    private fun buildJavalinServer(): Javalin {
        val configurator = javalinConfigurator.orElseGet { defaultJavalinConfigurator }
        val server = Javalin.create { configurator.configureJavalin(it) }!!
        registerWebRequestHandlers(server)
        return server
    }

    private fun registerWebRequestHandlers(server: Javalin) {
        for (handler in webRequestHandlers) {
            val wrapper = RequestHandlerWrapperWithContext(handler, this, dbApi, jobScheduler)
            server.addHandler(handler.handlerType, handler.path) { wrapper.handle(it) }
        }
    }

    private fun buildScheduledJobManager(): ScheduledJobManager {
        val manager = ScheduledJobManagerImpl(this, dbApi)
        for (job in scheduledJobs) {
            manager.registerNewJob(job)
        }
        return manager
    }
}
