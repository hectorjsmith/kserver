package org.hsmith.kserver.api.impl

import java.util.*
import org.hsmith.hibernate_decorator.api.DbApi
import org.hsmith.kserver.api.KServer
import org.hsmith.kserver.api.KServerBuilder
import org.hsmith.kserver.javalin.config.JavalinConfigurator
import org.hsmith.kserver.javalin.handler.WebRequestHandler
import org.hsmith.kserver.scheduler.base.ScheduledJob

internal class KServerBuilderImpl() : KServerBuilder {
    private var port = Optional.empty<Int>()
    private var dbApi = Optional.empty<DbApi>()
    private var javalinConfigurator = Optional.empty<JavalinConfigurator>()
    private val webRequestHandlerList = mutableListOf<WebRequestHandler>()
    private val scheduledJobList = mutableListOf<ScheduledJob>()

    override fun withServerPort(port: Int): KServerBuilder {
        this.port = Optional.of(port)
        return this
    }

    override fun withDbApi(dbApi: DbApi): KServerBuilder {
        this.dbApi = Optional.of(dbApi)
        return this
    }

    override fun withJavalinConfigurator(javalinConfigurator: JavalinConfigurator): KServerBuilder {
        this.javalinConfigurator = Optional.of(javalinConfigurator)
        return this
    }

    override fun withRequestHandler(handler: WebRequestHandler): KServerBuilder {
        webRequestHandlerList.add(handler)
        return this
    }

    override fun withRequestHandler(handlers: Iterable<WebRequestHandler>): KServerBuilder {
        webRequestHandlerList.addAll(handlers)
        return this
    }

    override fun withScheduledJob(job: ScheduledJob): KServerBuilder {
        scheduledJobList.add(job)
        return this
    }

    override fun withScheduledJob(jobs: Iterable<ScheduledJob>): KServerBuilder {
        scheduledJobList.addAll(jobs)
        return this
    }

    override fun build(): KServer {
        return KServerImpl(port, dbApi, javalinConfigurator, webRequestHandlerList, scheduledJobList)
    }
}
