package org.hsmith.kserver.api

/**
 * Main server class. When started this will setup and start a [io.javalin.Javalin] server and a Quartz executor.
 */
interface KServer {
    /**
     * Flag to check if the server is already running or not.
     */
    val isRunning: Boolean

    /**
     * Start the [KServer] instance. This will setup and start the backing [io.javalin.Javalin] server and the Quartz
     * job executor.
     */
    fun start()

    /**
     * Stop the [KServer] instance.
     */
    fun stop()
}
