package org.hsmith.kserver.api

import org.hsmith.kserver.api.impl.KServerBuilderImpl
import org.hsmith.kserver.scheduler.trigger.QuartzTriggerFactory
import org.hsmith.kserver.scheduler.trigger.QuartzTriggerFactoryImpl

/**
 * Main API for the kserver library.
 * Includes methods for building high level objects to use this library.
 */
object KServerApi {
    /**
     * Create a new builder object to build a [KServer] instance.
     */
    fun newKServerBuilder(): KServerBuilder {
        return KServerBuilderImpl()
    }

    /**
     * Create a new factory instance to build Quartz triggers.
     *
     * See [org.quartz.Trigger].
     */
    fun newQuartzTriggerFactory(): QuartzTriggerFactory {
        return QuartzTriggerFactoryImpl()
    }
}
