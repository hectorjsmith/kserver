package org.hsmith.kserver.javalin.handler

import io.javalin.http.HandlerType
import org.hsmith.kserver.javalin.context.WebRequestContext

/**
 * Web request handler.
 * Implementations of this interface are responsible for responding to incoming web requests.
 */
interface WebRequestHandler {
    /**
     * URL path where this handler responds to.
     * The path can include placeholders for path variables. See [Javalin Docs for path variables](https://javalin.io/documentation#handlers).
     */
    val path: String

    /**
     * Type of requests this handler responds to (for example: GET, POST, PUT).
     *
     * See [HandlerType].
     */
    val handlerType: HandlerType

    /**
     * Function that actually handles the actual web request from Javalin.
     *
     * See [WebRequestContext] for details on the data provided to this method.
     */
    fun handle(context: WebRequestContext)
}
