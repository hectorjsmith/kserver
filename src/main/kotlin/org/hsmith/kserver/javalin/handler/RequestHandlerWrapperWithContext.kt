package org.hsmith.kserver.javalin.handler

import io.javalin.http.Context
import java.util.*
import org.hsmith.hibernate_decorator.api.DbApi
import org.hsmith.kserver.api.KServer
import org.hsmith.kserver.javalin.context.WebRequestContextImpl
import org.hsmith.kserver.scheduler.scheduler.JobScheduler

internal class RequestHandlerWrapperWithContext(
    private val handler: WebRequestHandler,
    private val server: KServer,
    private val dbApi: Optional<DbApi>,
    private val jobScheduler: Optional<JobScheduler>
) {
    fun handle(javalinContext: Context) {
        val session = Optional.ofNullable(dbApi.orElse(null)?.newSession())
        val requestContext = WebRequestContextImpl(javalinContext, server, session, jobScheduler)
        handler.handle(requestContext)
        session.ifPresent { it.close() }
    }
}
