package org.hsmith.kserver.javalin.context

import io.javalin.http.Context
import java.util.*
import org.hsmith.hibernate_decorator.api.DbSession
import org.hsmith.kserver.api.KServer
import org.hsmith.kserver.scheduler.scheduler.JobScheduler

internal class WebRequestContextImpl(
    override val javalinContext: Context,
    override val server: KServer,
    override val dbSession: Optional<DbSession>,
    override val jobScheduler: Optional<JobScheduler>
) : WebRequestContext
