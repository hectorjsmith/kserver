package org.hsmith.kserver.javalin.context

import io.javalin.http.Context
import java.util.*
import org.hsmith.hibernate_decorator.api.DbSession
import org.hsmith.kserver.api.KServer
import org.hsmith.kserver.scheduler.scheduler.JobScheduler

/**
 * Context object that allows providing data to each webrequest handler when it is invoked.
 */
interface WebRequestContext {
    /**
     * Raw [io.javalin.Javalin] context object that includes data about the actual request.
     */
    val javalinContext: Context

    /**
     * [KServer] instance this web request handler was registered on.
     * This interface allows starting and stopping the server instance.
     *
     * Note that once the server is stopped, no incoming web requests will be handled.
     */
    val server: KServer

    /**
     * Optional database session from the `hibernate-decorator` library.
     * If a [org.hsmith.hibernate_decorator.api.DbApi] was provided when the [org.hsmith.kserver.api.KServer]
     * was build, each web request will be provided with a fresh database session. Otherwise, this property will
     * always be empty.
     *
     * Note that the database session is automatically closed when the request handling is complete.
     *
     * See [org.hsmith.kserver.api.KServerBuilder.withDbApi].
     */
    val dbSession: Optional<DbSession>

    /**
     * Optional job scheduler instance.
     * This interface is used to register new Quartz jobs at runtime.
     */
    val jobScheduler: Optional<JobScheduler>
}
