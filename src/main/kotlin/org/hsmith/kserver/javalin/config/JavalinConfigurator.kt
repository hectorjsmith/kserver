package org.hsmith.kserver.javalin.config

import io.javalin.core.JavalinConfig

/**
 * Configurator class to handle configuring [io.javalin.Javalin] server before startup.
 */
interface JavalinConfigurator {
    /**
     * Configure the [io.javalin.Javalin] server before startup.
     * This method takes a JavalinConfig instance that can be used to set options for the [io.javalin.Javalin] server.
     */
    fun configureJavalin(javalinConfig: JavalinConfig)
}
