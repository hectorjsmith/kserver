package org.hsmith.kserver.javalin.config

import io.javalin.core.JavalinConfig
import io.javalin.http.Context
import org.slf4j.LoggerFactory

class DefaultJavalinConfigurator : JavalinConfigurator {
    private val logger = LoggerFactory.getLogger(this::class.java)

    override fun configureJavalin(javalinConfig: JavalinConfig) {
        javalinConfig.requestLogger { ctx: Context, executionTimeMs: Float -> requestLoggerHandler(ctx, executionTimeMs) }
        javalinConfig.showJavalinBanner = false
    }

    private fun requestLoggerHandler(ctx: Context, executionTimeMs: Float) {
        logger.info(String.format("%s\t%s\t%fms", ctx.req.method, ctx.req.requestURI, executionTimeMs))
    }
}
