# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

_No unreleased changes_

## [0.2.0] - 2020-11-10

### Added
- Added `JobScheduler` interface to web request handlers to allow scheduling new jobs (#6)
- Added `KServer` interface to web request context and scheduled job context to allow shutting down the server (#7)
- Added support for scheduling jobs using a cron expression (#13)

## [0.1.0] - 2020-11-03

- Initial release
