#!/bin/bash

# Build main project
./gradlew clean jar

cd examples

# Run Kotlin example
./gradlew runKotlinExample
