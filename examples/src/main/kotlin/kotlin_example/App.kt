package kotlin_example

import io.javalin.http.HandlerType
import io.javalin.http.InternalServerErrorResponse
import io.ktor.client.*
import io.ktor.client.engine.apache.*
import io.ktor.client.request.*
import org.hsmith.hibernate_decorator.api.HibernateDecoratorApi
import org.hsmith.kserver.api.KServerApi
import org.slf4j.LoggerFactory
import kotlinx.coroutines.*
import org.hsmith.hibernate_decorator.data.entity.impl.EntityWithLongIdBase
import org.hsmith.kserver.javalin.context.WebRequestContext
import org.hsmith.kserver.javalin.handler.WebRequestHandler
import org.hsmith.kserver.scheduler.base.ScheduledJob
import org.hsmith.kserver.scheduler.context.ScheduledJobContext
import org.quartz.Trigger
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table

class App {
    private val logger = LoggerFactory.getLogger(App::class.java)
    private val serverPort = 34712

    fun run() {
        logger.info("Running kotlin example project")
        val dbApi = HibernateDecoratorApi.newDbApiBuilder().build()
        val server = KServerApi.newKServerBuilder()
                .withDbApi(dbApi)
                .withServerPort(serverPort)
                .withRequestHandler(AddEntityWebRequest())
                .withRequestHandler(ShutdownWebRequest())
                .withScheduledJob(LogScheduledJob())
                .build()

        makeDelayedRequest(2_000L)
        makeDelayedRequest(5_000L)
        makeDelayedRequest(7_500L)
        stopServerAfterDelay(10_000L)
        server.start()
    }

    private fun stopServerAfterDelay(delayTime: Long) {
        GlobalScope.launch {
            delay(delayTime)
            val client = HttpClient(Apache)
            client.get<String> {
                url("http://127.0.0.1:$serverPort/shutdown")
            }
            client.close()
        }
    }

    private fun makeDelayedRequest(delayTime: Long) {
        GlobalScope.launch {
            delay(delayTime)
            val client = HttpClient(Apache)

            val message = client.get<String> {
                url("http://127.0.0.1:$serverPort/add")
            }

            logger.info("CLIENT: Message from the server: $message")

            client.close()
        }
    }
}

class AddEntityWebRequest : WebRequestHandler {
    private val logger = LoggerFactory.getLogger(this::class.java)
    override val handlerType: HandlerType = HandlerType.GET
    override val path: String = "/add"

    override fun handle(context: WebRequestContext) {
        if (context.dbSession.isEmpty) {
            logger.error("No database session available")
            throw InternalServerErrorResponse("No database session available")
        }

        val session = context.dbSession.get()
        val repo = session.newRepository(DatabaseEntity::class.java)
        val entity = DatabaseEntity()
        entity.stringField = "some value"
        repo.save(entity)

        context.javalinContext.result("SUCCESS")
    }
}

class ShutdownWebRequest : WebRequestHandler {
    private val logger = LoggerFactory.getLogger(this::class.java)
    override val handlerType: HandlerType = HandlerType.GET
    override val path: String = "/shutdown"

    override fun handle(context: WebRequestContext) {
        logger.info("Scheduling server shutdown")
        context.jobScheduler.ifPresent { it.scheduleJob(ShutdownScheduledJob()) }
    }
}

class LogScheduledJob : ScheduledJob {
    private val logger = LoggerFactory.getLogger(this::class.java)
    private val triggerFactory = KServerApi.newQuartzTriggerFactory()

    override val jobGroup: String = "sample.job"
    override val jobName: String = "job1"
    override val jobTriggers: List<Trigger> by lazy {
        listOf(
            triggerFactory.newSimpleRecurringTrigger(jobGroup, jobName, 500, 1000),
            triggerFactory.newOnceOffTrigger(jobGroup, jobName, 2750),
            triggerFactory.newCrontabTrigger(jobGroup, jobName, "*/5 * * ? * *")
        )
    }

    override fun run(context: ScheduledJobContext) {
        val fireTime = context.jobExecutionContext.fireTime.toInstant().toEpochMilli()
        val scheduledTime = context.jobExecutionContext.scheduledFireTime.toInstant().toEpochMilli()
        val timeDelay = fireTime - scheduledTime
        val triggerId = context.jobExecutionContext.trigger.key.name

        if (context.dbSession.isEmpty) {
            logger.error("No database session available")
            return
        }

        val session = context.dbSession.get()
        val repo = session.newRepository(DatabaseEntity::class.java)
        val entityCount = repo.count()
        logger.info("Running job $jobName - found $entityCount entities in the database (job fired ${timeDelay}ms after scheduled time) - Trigger: $triggerId")
    }
}

class ShutdownScheduledJob : ScheduledJob {
    private val logger = LoggerFactory.getLogger(this::class.java)
    private val triggerFactory = KServerApi.newQuartzTriggerFactory()

    override val jobGroup: String = "sample.job"
    override val jobName: String = "shutdown"
    override val jobTriggers: List<Trigger> by lazy {
        listOf(
            triggerFactory.newOnceOffTrigger(jobGroup, jobName, 500)
        )
    }

    override fun run(context: ScheduledJobContext) {
        logger.info("Running job $jobName after web request")
        context.server.stop()
    }
}

@Entity
@Table(name = "db_entity")
class DatabaseEntity : EntityWithLongIdBase() {
    @Column(name = "string_field", length = 1000, nullable = false)
    var stringField: String = ""
}

fun main() {
    App().run()
}
