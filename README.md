# KServer

Kotlin library that makes it easy to get a webserver up and running.

[![pipeline status](https://gitlab.com/hectorjsmith/kserver/badges/main/pipeline.svg)](https://gitlab.com/hectorjsmith/kserver/-/commits/main)

This library automatically configures [Javalin](https://javalin.io/) for web request handling, [Quartz](https://www.quartz-scheduler.org/) for scheduled jobs, and [Hibernate](https://hibernate.org/) & [Hibernate Decorator](https://gitlab.com/hectorjsmith/hibernate-decorator) for database access.

---

## Table of Contents

1. Add dependency
2. Setup database (optional)
3. Create KServer instance
4. Web request handlers
    1. Web request handler context
5. Scheduled jobs
    1. Scheduled job context
6. Examples

## 1. Add dependency

Find the latest compiled packages for this project on the [Gitlab Package Registry](https://gitlab.com/hectorjsmith/kserver/-/packages).

Include this library in your project by adding the Gitlab Package Registry as a maven repository to your build.gradle file.

```groovy
repositories {
    maven {
        url "https://gitlab.com/api/v4/projects/22089104/packages/maven"
    }
}
```

Then add a dependency on the library itself.

```groovy
dependencies {
    implementation 'org.hsmith:kserver:0.2.0'
}
```

Note that you will also need to add dependencies for Hibernate, Hibernate Decorator, Quartz, and Javalin for the library to function correctly.

See the `build.gradle` file in the included [example project](https://gitlab.com/hectorjsmith/kserver/-/blob/main/examples/build.gradle) for an example of this.

## 2. Setup database (optional)

The first step is to set up database access. See the [hibernate-decorator](https://gitlab.com/hectorjsmith/hibernate-decorator) project for more details on this.

```kotlin
val dbApi = HibernateDecoratorApi.newDbApiBuilder().build()
```

Note that this step is optional, the library can function without a database.

By providing a database instance, this library will automatically open a new database session when processing web requests and firing scheduled jobs.

## 3. Create KServer instance

Create a new `KSserver` instance by using the provided builder class.

```kotlin
val server = KServerApi.newKServerBuilder()
    .withDbApi(dbApi)
    .withServerPort(7000)
    .withRequestHandler(WebHandlerOne())
    .withScheduledJob(JobNumberOne())
    .withScheduledJob(JobNumberTwo())
    .build()
```

Once the `KServer` instance has been built, start the server with the `start()` method.

```kotlin
server.start()
```

## 4. Web request handlers

All web request handlers must implement the `WebRequestHandler` interface.

```kotlin
class ExampleHandler : WebRequestHandler {
    override val handlerType: HandlerType = HandlerType.GET
    override val path: String = "/example"

    override fun handle(context: WebRequestContext) {
    }
}
```

The `handlerType` property determines what types of request this handler will respond to. Valid options include `GET`, `POST`, `PUT`, `DELETE`.

The `path` property determines the path this handler responds to. See the [Javalin documentation](https://javalin.io/documentation#handlers) for more information.

Register the handler when building the `KServer` instance using the `withRequestHandler()` method.

It is not possible to add request handlers to an existing `KServer` instance.

## 4.1. Web request handler context

This library creates a new `WebRequestContext` object for each web request. The web request handler can use this context object when handling the request.

The context object includes:
  * The raw Javalin context object
  * An optional `DbSession` database session
  * An optional `JobScheduler` interface for scheduling jobs
  * The `KServer` instance where the handler was registered
  
Note that the database session will only be present if the `KServer` was built with a `DbApi` instance.

The Javalin context object includes methods to get data about the request (request body, path params, etc) and methods to respond to the request. See the [Javalin documentation](https://javalin.io/documentation#context) for more information on the context object.

The database session allows the handler to read and write data to the database. Note that the library automatically closes this database session after the web request has been handled.

The `JobScheduler` interface allows jobs to be scheduled when processing web requests. Jobs scheduled in this way have the same requirements and behave in the same way as jobs scheduled on `KServer` creation.

The `KServer` instance can be used to stop the server.

## 5. Scheduled jobs

All scheduled jobs must implement the `ScheduledJob` interface.
Each job controls when it is executed by returning a list of `Trigger` instances.
Quartz uses these triggers when scheduling the job.

```kotlin
class ExampleJob : ScheduledJob {
    private val triggerFactory = KServerApi.newQuartzTriggerFactory()

    override val jobGroup: String = "example.job"
    override val jobName: String = "job1"
    override val jobTriggers: List<Trigger> by lazy {
        listOf(
            triggerFactory.newSimpleRecurringTrigger(jobGroup, jobName, 500, 1000),
            triggerFactory.newOnceOffTrigger(jobGroup, jobName, 2750)
        )
    }

    override fun run(context: ScheduledJobContext) {
    }
}
```

The `KServerApi.newQuartzTriggerFactory()` method returns a new trigger factory for scheduled jobs.
This factory simplifies the creation of common types of triggers.

The `jobGroup` and `jobName` properties make up the ID of each job.
Each job must have a unique ID.

The `jobTriggers` property tells Quartz when to fire the job.
Triggers can be used to fire the job a single time or to fire the job multiple times.

Register scheduled jobs when building the `KServer` instance using the `withScheduledJob()` method.

It is possible to schedule new jobs while `KServer` is running.

## 5.1. Scheduled job context

This library creates a new `ScheduledJobContext` object for each job execution.

The context object includes:
  * The raw Quartz `JobExecutionContext` object
  * An optional `DbSession` database session
  * The `KServer` instance where the job was scheduled

Note thtat the database session will only be present if the `KServer` was built with a `DbApi` instance.

The `JobExecutionContext` object includes all the data about the current job execution, including details about the job itself, the exact time the job fired, the next scheduled execution time, etc.
See the [Quartz documentation](http://www.quartz-scheduler.org/api/2.1.7/org/quartz/JobExecutionContext.html) for more details.

The database session allows the job to read and write data to the database.
Note that the library automatically closes the database session after the job completes.

The `KServer` instance can be used to stop the server.

## 6. Examples

The project includes an [example project](https://gitlab.com/hectorjsmith/kserver/-/tree/main/examples) that demonstrates how to use this library.
